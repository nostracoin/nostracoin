// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2015 The Bitcoin Core developers
// Copyright (c) 2015-2019 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

// flags = randomx_get_flags(); // this requires huge pages support
//  sudo sysctl -w vm.nr_hugepages=1280
//  Permanent huge pages reservation
//  sudo bash -c "echo vm.nr_hugepages=1280 >> /etc/sysctl.conf"

#include "primitives/block.h"


#include "arith_uint256.h"
#include "crypto/common.h"
#include "hashwrapper.h"


#include "tinyformat.h"
#include "utilstrencodings.h"
#include <cstdio>
#include <iomanip>
#include <iostream>


// uint256 CBlockHeader::GetHash() const { return SerializeHash(*this); }

randomx_cache *CBlockHeader::myCache = nullptr;
randomx_cache *CBlockHeader::miningCache = nullptr;
randomx_vm *CBlockHeader::myMachine = nullptr;
randomx_vm *CBlockHeader::miningMachine = nullptr;

randomx_dataset *CBlockHeader::miningDataset = nullptr;
randomx_flags CBlockHeader::flags;
randomx_flags CBlockHeader::miningflags;

uint256 CBlockHeader::myKey;
uint256 CBlockHeader::miningKey;

// mining mode can't access chainActive and mapBlockIndex

uint256 CBlockHeader::GetRandomXBlockHashMiner(uint256 key, bool lowMemoryMining) const
{
    // https://github.com/tevador/RandomX#proof-of-work
    if (miningKey != key){
        miningflags = randomx_get_flags();
        //miningflags |= RANDOMX_FLAG_LARGE_PAGES; //usually want to use large pages, but this causes cache allocation error if large pages not possible.
        
        if (!lowMemoryMining)
        {
            miningflags |= RANDOMX_FLAG_FULL_MEM;
            std::cout << "High Memory Mining" << std::endl;
        }
        if (miningMachine != nullptr)
        {
            if (lowMemoryMining){
                randomx_release_cache(miningCache);
            }else{
                randomx_release_dataset(miningDataset);
            }
            randomx_destroy_vm(miningMachine);
        }
        //std::cout << "Mining Old Key " << miningKey.ToString() << std::endl;
        std::cout << "Mining/Verification New RandomXRN Key " << key.ToString() << std::endl;
        miningKey = key;

        //std::cout << "Allocate Cache" << std::endl;
        miningCache = randomx_alloc_cache(miningflags);
        if (miningCache == nullptr)
        {
            std::cout << "Cache allocation failed. This should never happen" << std::endl;
            assert(1 == 2);
            return uint256S("");
        }

        randomx_init_cache(miningCache, &key, sizeof key); // see above comment
        //std::cout << "Initialized Cache" << std::endl;

        if (lowMemoryMining){
            //std::cout << "Create VM" << std::endl;
            miningMachine = randomx_create_vm(miningflags, miningCache, NULL);
        }
        else{
            //std::cout << "Allocate Dataset" << std::endl;
            miningDataset = randomx_alloc_dataset(miningflags);
            if (miningDataset == nullptr)
            {
                std::cout << "Dataset allocation failed" << std::endl;
                assert(1 == 2);
                return uint256S("");
            }

            auto datasetItemCount = randomx_dataset_item_count();

            //std::cout << "Initialize Dataset" << std::endl;
            randomx_init_dataset(miningDataset, miningCache, 0, datasetItemCount);
            randomx_release_cache(miningCache);

            //std::cout << "Create VM" << std::endl;
            miningMachine = randomx_create_vm(miningflags, NULL, miningDataset);
        }

        if (miningMachine == nullptr)
        {
            std::cout << "Failed to create a virtual machine" << std::endl;
            assert(1 == 2);
            return uint256S("");
        }
        //std::cout << "VM Created" << std::endl;
    }

    //This code can be used to test if randomx is working
    //randomx_calculate_hash doesn't seem to work if the code is called too early - like for the genesis block.
    //error Program received signal SIGSEGV, Segmentation fault.
    //0x0000555555acd37f in randomx::JitCompilerX86::generateProgramPrologue(randomx::Program&, randomx::ProgramConfiguration&) ()
    /*const char myKey2[] = "RandomX example key";
    const char myInput2[] = "RandomX example input";
    char hash2[RANDOMX_HASH_SIZE];
    std::cout << "1" << std::endl;
    randomx_flags flags2 = randomx_get_flags();
    std::cout << "2" << std::endl;
    randomx_cache *myCache2 = randomx_alloc_cache(flags2);
    std::cout << "3" << std::endl;
    randomx_init_cache(myCache2, &myKey2, sizeof myKey2);
    std::cout << "4" << std::endl;
    randomx_vm *myMachine2 = randomx_create_vm(flags2, myCache2, NULL);
    std::cout << "5" << std::endl;
    randomx_calculate_hash(myMachine2, &myInput2, sizeof myInput2, hash2);
    std::cout << "6" << std::endl;
    randomx_destroy_vm(myMachine2);
    randomx_release_cache(myCache2);
    for (unsigned i = 0; i < RANDOMX_HASH_SIZE; ++i)
        printf("%02x", hash2[i] & 0xff);
    printf("\n");*/

    uint256 hash;
    //std::cout << "Calculate Hash: nVersion " << nVersion << " MyKey " << miningKey.ToString() << std::endl;
    randomx_calculate_hash(miningMachine, BEGIN(nVersion), 80, &hash);

    // output hash to screen
    hash = ArithToUint256(UintToArith256(hash) >> 28);
    return hash;
}

/*uint256 CBlockHeader::GetHash() const
{
    // This should never happen
    assert(1 == 2);
    return uint256S("");
}*/

#include "aescache.h"
CBlockAesCache *CBlockHeader::aesCache;

/*void CBlockHeader::init() const
{
    

    
}*/


// uint256 CBlockHeader::GetHash() const { return SerializeHash(*this); }


uint256 CBlockHeader::GetHash() const
{

    if (hashPrevBlock == uint256S("0000000000000000000000000000000000000000000000000000000000000000"))
    {
        return uint256S("00000000009ce9acb93bc86543c2107195a1432bce04d29d98e861a9e2968b73");
    }
    //return GetRandomXBlockHashMiner(GetRandomXKey(), true);
    uint256 hash;
    uint256 orig_hash = SerializeHash(*this);

    if (!aesCache)
    { 
        std::cout << "Loading CBlockAesCache Hash Cache" << std::endl;
        aesCache = new CBlockAesCache(GetDataDir() / "blocks" / "cache", 1, false, false);
    }
    if (aesCache)
    {
        //std::cout << "Looking in Cache" << std::endl;
        
        if (!aesCache->ReadHash(orig_hash, hash))
        {
            //std::cout << "Not found in cache, calculating and writing to cache" << std::endl;
            //std::cout << hashPrevBlock.ToString() << ' ' << hashMerkleRoot.ToString() << std::endl;
            hash = GetRandomXBlockHashMiner(GetRandomXKey(), true);
            //GetRandomXBlockHashNoCache();
            aesCache->WriteHash(orig_hash, hash);
        }
    }
    else
    {
        std::cout << "No Cache" << std::endl;
        hash = GetRandomXBlockHashMiner(GetRandomXKey(), true);
        //GetRandomXBlockHashNoCache();
    }

    return hash;
}


uint256 CBlockHeader::GetRandomXKey() const
{
    // New key each day
    uint64_t newkeyheight = nTime / (60 * 60 * 24);
    std::string keyString = "1968 " + std::to_string(newkeyheight) + " 1968";
    return SerializeHash(keyString);
}



std::string CBlock::ToString() const
{
    std::stringstream s;
    s << strprintf("CBlock(hash=%s, ver=%d, hashPrevBlock=%s, hashMerkleRoot=%s, nTime=%u, nBits=%08x, nNonce=%u)\n",
        GetHash().ToString(), nVersion, hashPrevBlock.ToString(), hashMerkleRoot.ToString(), nTime, nBits, nNonce);
    for (unsigned int i = 0; i < vtx.size(); i++)
    {
        //   s << "  " << vtx[i]->ToString() << "\n";
    }
    return s.str();
}

uint64_t CBlock::GetBlockSize() const
{
    if (nBlockSize == 0)
        nBlockSize = ::GetSerializeSize(*this, SER_NETWORK, PROTOCOL_VERSION);
    return nBlockSize;
}


arith_uint256 GetWorkForDifficultyBits(uint32_t nBits)
{
    arith_uint256 bnTarget;
    bool fNegative;
    bool fOverflow;
    bnTarget.SetCompact(nBits, &fNegative, &fOverflow);
    if (fNegative || fOverflow || bnTarget == 0)
        return 0;
    // We need to compute 2**256 / (bnTarget+1), but we can't represent 2**256
    // as it's too large for a arith_uint256. However, as 2**256 is at least as large
    // as bnTarget+1, it is equal to ((2**256 - bnTarget - 1) / (bnTarget+1)) + 1,
    // or ~bnTarget / (nTarget+1) + 1.
    return (~bnTarget / (bnTarget + 1)) + 1;
}
