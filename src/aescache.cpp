#include "aescache.h"
#include "dbwrapper.h"

//CBlockAesCache *aesCache = NULL;

CBlockAesCache::CBlockAesCache(const fs::path &path, size_t nCacheSize, bool fMemory, bool fWipe) : CDBWrapper(path, nCacheSize, fMemory, fWipe, false) {
}

bool CBlockAesCache::WriteHash(const uint256 &hash, const uint256 &aesHash)
{
    return Write(std::make_pair('y', hash), aesHash);
}

bool CBlockAesCache::ReadHash(const uint256 &hash, uint256 &aesHash)
{
    return Read(std::make_pair('y', hash), aesHash);
}