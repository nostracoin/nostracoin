strip src/bitcoind
mv src/bitcoind releasebins/linux/repnetd

strip src/bitcoin-cli
mv src/bitcoin-cli releasebins/linux/repnet-cli

strip src/bitcoin-miner
mv src/bitcoin-miner releasebins/linux/repnet-miner

strip src/qt/bitcoin-qt
mv src/qt/bitcoin-qt releasebins/linux/repnet-qt

strip src/bitcoind.exe
mv src/bitcoind.exe releasebins/windows/repnetd.exe

strip src/bitcoin-cli.exe
mv src/bitcoin-cli.exe releasebins/windows/repnet-cli.exe

strip src/bitcoin-miner.exe
mv src/bitcoin-miner.exe releasebins/windows/repnet-miner.exe

strip src/qt/bitcoin-qt.exe
mv src/qt/bitcoin-qt.exe releasebins/windows/repnet-qt.exe

